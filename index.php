<?php
require "bootstrap.php";
use Chatter\Models\User;
use Chatter\Models\Message;

$app = new \Slim\App();
$app->get('/customers/{id}', function($request, $response, $array){
    $id = $request->getAttribute('id');
    $json = '{"1":"John", "2":"Jack","3":"Shelly","4":"Amir"}';
    $array = json_decode($json, true);

    if(array_key_exists($id, $array)){
        $name = $array[$id];
        return $response->write('Hello, '.$name);
    }
    else{
        return $response->write('Hello, id does not exist');
    }
});

//שליפת נתונים
$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();

    $payload = [];
    foreach($users as $u){
        $payload[$u->id] = [
            "username" => $u->username,
            "email" => $u->email
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//עדכון
$app->post('/users', function($request, $response,$args){
    $email = $request->getParsedBodyParam('email','');
    $username = $request->getParsedBodyParam('username','');
    $_user = new User();
    $_user->email = $email;
    $_user->username = $username;
    $_user->save();
       
    if($_user->id){
        $payload = ['user_id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

//מחיקה
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']);
    $user->delete();
    
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});

//הוספת קבוצה
$app->post('/users/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody();
        
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

//מחיקת קבוצה
//$app->delete('/users/bulk/{payload:.*}', function($request, $response, $args){
/*$app->delete('/users/bulk/{id:.*}', function($request, $response, $args){
    //$payload = $request->getAttribute('payload');
    $payload = explode('/', $request->getAttribute('id'));
    //$payload = $request->getParsedBody()['id'];
    die($payload);
    $_user = new User();
    $users = $_user->all();
    $array = json_decode($users, true);

    foreach($array as $u){
        foreach($payload as $p){
            if($u[id] == $d){
                $u->delete();
            }
        }
    }
    if($payload->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});*/

//הרשאה לאבטחת מידע same origin policy

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();
